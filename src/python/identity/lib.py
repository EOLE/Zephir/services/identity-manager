from .query import (list_all_settings,
                    fetch_settings,
                    upsert_settings,
                    erase_settings
                   )

class Identity():
    """Identity Manage API
    """

    def get_settings(self, cursor, username: str) -> dict:
        """Returns: user settings
        """
        return fetch_settings(cursor, username)

    def set_settings(self,
                     cursor,
                     username: str,
                     settings: dict) -> int:
        """Returns: Identity Id
        """
        return upsert_settings(cursor, username, settings)

    def erase_settings(self, cursor):
        erase_settings(cursor)
