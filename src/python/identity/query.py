from zephir.i18n import _
from json import dumps
"""
List all settings
"""
FETCH_ALL_SETTINGS = '''
    SELECT identitysettings
    FROM identity
 '''

"""
Get a user settings
"""
FETCH_SETTINGS = '''
    SELECT identityid, identitysettings
    FROM identity
    WHERE identityusername = %s
 '''

"""
Creates a new user settings
"""
INSERT_SETTINGS = '''
    INSERT INTO identity (identityusername, identitysettings)
    VALUES (%s, %s)
    RETURNING identityid
'''

"""
Update user settings
"""
UPDATE_SETTINGS = '''
    UPDATE identity SET identitysettings=%s
    WHERE identityid=%s
    RETURNING identityid
'''

"""
Suppression du contenu de la table identity
"""
IDENTITY_ERASE = '''DELETE from identity'''


def list_all_settings(cursor):
    cursor.execute(FETCH_ALL_SETTINGS)
    ret = []
    for server in cursor.fetchall():
        ret.append(server)
    return ret


def fetch_settings(cursor, username):
    cursor.execute(FETCH_SETTINGS, (username,))
    fetched = cursor.fetchone()
    if fetched is None:
        raise Exception(_('unable to find a server with username {}').format(username))
    return fetched[1]


def upsert_settings(cursor, username, usersettings):
    cursor.execute(FETCH_SETTINGS, (username,))
    fetched = cursor.fetchone()
    if fetched is None:
        cursor.execute(INSERT_SETTINGS, (username, dumps(usersettings)))
    else:
        fetched[1].update(usersettings)
        cursor.execute(UPDATE_SETTINGS, (dumps(fetched[1]), fetched[0]))
    return cursor.fetchone()

def erase_settings(cursor):
    """
    Supprime le contenu de la table Identity
    """
    cursor.execute(IDENTITY_ERASE)
