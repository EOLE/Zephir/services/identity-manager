from zephir.config import ServiceConfig
from identity import Identity
from zephir.database import connect
from util import setup_module, teardown_module
import json


def setup_function(function):
    global identity, cursor
    cursor = CONN.cursor()
    identity = Identity()
    identity.erase_settings(cursor)


def test_create_settings():
    global identity, cursor
    username = "yo"
    settings = {"username": "yo", "settings": {"ssh_pubkey": "654987", "lastname": "Dupont", "firstname": "Philippe"}}
    id = identity.set_settings(cursor, username, json.dumps(settings))
    assert id == 1


def test_get_settings():
    global identity, cursor
    username = "yo"
    settings = {"username": "yo", "settings": {"ssh_pubkey": "654987", "lastname": "Dupont", "firstname": "Philippe"}}
    id = identity.set_settings(cursor, username, json.dumps(settings))
    retour = identity.get_settings(cursor, username)
    assert retour == settings


def test_update_settings():
    global identity, cursor
    username = "yo"
    settings = {"username": "yo", "settings": {"ssh_pubkey": "654987", "lastname": "Dupont", "firstname": "Philippe"}}
    settings2 = {"username": "yo", "settings": {"ssh_pubkey": "111222", "lastname": "koko", "firstname": "Jamel"}}
    id = identity.set_settings(cursor, username, json.dumps(settings))
    id2 = identity.set_settings(cursor, username, json.dumps(settings2))
    assert id == id2
    retour = identity.get_settings(cursor, username)
    assert retour == settings2
