-- Revert identity:identity_schema from pg

BEGIN;

DROP SCHEMA identity CASCADE;

COMMIT;
