-- Deploy identity:identity_schema to pg

BEGIN;

-- Identity table creation
CREATE TABLE Identity (
  IdentityId SERIAL PRIMARY KEY,
  IdentityUsername VARCHAR(255) NOT NULL UNIQUE,
  IdentitySettings JSON
);

COMMIT;